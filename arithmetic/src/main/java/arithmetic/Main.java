package arithmetic;

import org.redfx.strange.*;
import org.redfx.strange.algorithm.Classic;
import org.redfx.strange.gate.*;
import org.redfx.strange.local.*;
import org.redfx.strangefx.render.Renderer;

import java.util.*;

public class Main {

    static SimpleQuantumExecutionEnvironment sqee = new SimpleQuantumExecutionEnvironment();

    public static void main(String[] args) {
        multiplyModGate5x3mod6();
//        for (int a = 0; a < 4; a ++) {
//            for (int b = 0; b < 4; b++) {
//                int c =add(a,b); 
//                int d = blockadd(a, b);
//                System.err.println("ADDING "+a+" + "+b+" = "+c+" == "+d);
//            }
//        }
//        int cc = add(2,3, true);
//        int dd = blockadd(1,2, false);
//        System.err.println("cc = "+cc+" and dd = "+dd);
    }
    
    static int add(int a, int b) {
        return add(a, b, false);
    }
    
    static int add(int a, int b, boolean render) {
        if (a > 3) a = a%4;
        if (b > 3) b = b%4;
        Program p = new Program(7);
        Step prep = new Step ();
        if (a%2 ==1) prep.addGate(new X(1));
        if (b%2 ==1) prep.addGate(new X(2));
        
        if (a/2 ==1) prep.addGate(new X(4));
        if (b/2 ==1) prep.addGate(new X(5));
        
        Step s0 = new Step(new Toffoli(1, 2, 3));
        Step s1 = new Step(new Cnot(1, 2));
        Step s2 = new Step(new Toffoli(0, 2, 3));

        Step s3 = new Step(new Toffoli(4, 5, 6));
        Step s4 = new Step(new Cnot(4, 5));
        Step s5 = new Step(new Toffoli(3, 5, 6));

        Step s6 = new Step(new Cnot(4, 5));
        Step s7 = new Step(new Cnot(4, 5));
        Step s8 = new Step(new Cnot(3, 5));
        Step s9 = new Step(new Toffoli(0, 2, 3));
        Step s10 = new Step(new Cnot(1, 2));
        Step s11 = new Step(new Toffoli(1, 2, 3));

        Step s12 = new Step(new Cnot(1, 2));
        Step s13 = new Step(new Cnot(0, 2));

        p.addSteps(prep,s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11,s12, s13);
        Result result = sqee.runProgram(p);
        Qubit[] qubits = result.getQubits();
        int answer = qubits[6].measure()*4 + qubits[5].measure()*2+qubits[2].measure();
        if (render) {
            Renderer.renderProgram(p);
        }
        return answer;
    }

    static int blockadd(int a, int b) {
        return blockadd(a, b, false);
    }
    
    static int blockadd(int a, int b, boolean render) {
        if (a > 3) a = a%4;
        if (b > 3) b = b%4;
        Program p = new Program(7);
        Step prep = new Step ();
        if (a%2 ==1) prep.addGate(new X(1));
        if (b%2 ==1) prep.addGate(new X(2));
        
        if (a/2 ==1) prep.addGate(new X(4));
        if (b/2 ==1) prep.addGate(new X(5));
        
        Block carry = new Block(4);
        carry.addStep(new Step(new Toffoli(1, 2, 3)));
        carry.addStep(new Step(new Cnot(1, 2)));
        carry.addStep(new Step(new Toffoli(0,2,3)));
        
        Block sum = new Block(3);
        sum.addStep(new Step(new Cnot(1,2)));
        sum.addStep(new Step(new Cnot(0,2)));
        
        Block backCarry = new Block(4);
        backCarry.addStep(new Step(new Toffoli(0, 2, 3)));
        backCarry.addStep(new Step(new Cnot(1, 2)));
        backCarry.addStep(new Step(new Toffoli(1,2,3)));
        
        Step s0 = new Step(new BlockGate(carry, 0));
        Step s1 = new Step(new BlockGate(carry, 3));
        
        Step s2 = new Step(new Cnot(4,5));
        
        Step s3 = new Step(new BlockGate(sum, 3));
        Step s4 = new Step(new BlockGate(backCarry, 0));
        Step s5 = new Step(new BlockGate(sum, 0));

        Step i0 = new Step (new ProbabilitiesGate(0));
        
        Step i1 = new Step (new ProbabilitiesGate(0));
        Step i2 = new Step (new ProbabilitiesGate(0));
        Step i3 = new Step (new ProbabilitiesGate(0));
        p.addSteps(prep, i0, s0, i1, s1, i2, s2, s3, s4, i3, s5);
        Result result = sqee.runProgram(p);
        Qubit[] qubits = result.getQubits();
        int answer = qubits[6].measure()*4 + qubits[5].measure()*2+qubits[2].measure();
        if (render) {
            Renderer.renderProgram(p);
        }
        return answer;
    }
    
   public static void multiplyModGate5x3mod6() { // 5 x 3 mod 6 = 3
        Program p = new Program(9);
        int mul = 5;
        int N = 6;
        Step prep = new Step();
        prep.addGates(new X(4), new X(5)); // 3 in high register
        Step s = new Step(new MulModulus(0,3,mul, N));
        p.addStep(prep);
        p.addStep(s);
     long l0 = System.currentTimeMillis();
        Result result = sqee.runProgram(p);
        long l1 = System.currentTimeMillis();
        System.err.println("TIME = "+(l1-l0)/1000);

        Qubit[] q = result.getQubits();
        System.err.println("results: ");
        for (int i = 0; i < 9; i++) {
            System.err.println("m["+i+"]: "+q[i].measure());
        }
    }
}
